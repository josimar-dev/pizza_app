import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Store
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers, effects } from './store';

// components
import { ProductsComponent } from './containers/products/products.component';
import { ProductItemComponent } from './containers/product-tem/product-item.component';
import { PizzaDisplayComponent } from './components/pizza-display/pizza-display.component';
import { PizzaFormComponent } from './components/pizza-form/pizza-form.component';
import { PizzaItemComponent } from './components/pizza-item/pizza-item.component';
import { PizzaToppingsComponent } from './components/pizza-toppings/pizza-toppings.component';

// services
import { PizzasService } from './services/pizza.service';
import { ToppingService } from './services/topping.service';

// routes
export const ROUTES: Routes = [
  {
    path: '',
    component: ProductsComponent,
  },
  {
    path: 'new',
    component: ProductItemComponent,
  },
  {
    path: ':pizzaId',
    component: ProductItemComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature(effects),
    RouterModule.forChild(ROUTES),
  ],
  providers: [PizzasService, ToppingService],
  declarations: [
    ProductItemComponent,
    ProductsComponent,
    PizzaDisplayComponent,
    PizzaFormComponent,
    PizzaItemComponent,
    PizzaToppingsComponent,
  ],
  exports: [
    ProductItemComponent,
    ProductsComponent,
    PizzaDisplayComponent,
    PizzaFormComponent,
    PizzaItemComponent,
    PizzaToppingsComponent,
  ],
})
export class ProductsModule {}
