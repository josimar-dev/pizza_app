import { createReducer, on, Action } from '@ngrx/store';
import * as fromToppings from '../actions/toppings.action';
import { Topping } from '../../models/topping.model';
import { reducesArrayToEntity } from '../../../utils';

export interface ToppingsState {
  entities: { [id: number]: Topping };
  loaded: boolean;
  loading: boolean;
}

export const initialState: ToppingsState = {
  entities: {},
  loaded: false,
  loading: false,
};

const _toppingsReducer = createReducer(
  initialState,
  on(fromToppings.LOAD_TOPPINGS, (state) => ({ ...state, loading: true })),
  on(fromToppings.LOAD_TOPPINGS_FAIL, (state) => ({
    ...state,
    loading: false,
    loaded: false,
  })),
  on(fromToppings.LOAD_TOPPINGS_SUCCESS, (state, { payload }) => {
    const entities = reducesArrayToEntity(payload, state.entities);

    return {
      ...state,
      loading: false,
      loaded: true,
      entities: { ...entities },
    };
  })
);

export function toppingsReducer(state: ToppingsState, action: Action) {
  return _toppingsReducer(state, action);
}

// Toppings Selectors
export const getToppingsEntities = (state: ToppingsState) => state.entities;
export const getToppingsLoaded = (state: ToppingsState) => state.loaded;
export const getToppingsLoading = (state: ToppingsState) => state.loading;
