import { createSelector } from '@ngrx/store';
import { Pizza } from 'src/products/models/pizza.model';

import * as fromRoot from '../../../app/store';
import * as fromFeature from '../reducers';
import * as fromPizzas from '../reducers/pizzas.reducer';
// Because we told the StoreModule.forFeature method that our state for this module would
// be called "products", the pizzas object was wrapped in an object called products.
// Though it's just a reference for selectors, what really gets merged into the Store is the pizzas object,
// since it's the only reducer we have for this module

/**
 * CURRENT STATE FOR THE MODULE
 *
 * state = products: {
 *  pizzas: {
 *    data: [],
 *    loaded: false,
 *    loaded: false
 *  }
 * }
 *
 */

// Products State
// createSelector - allows us to traverse between the state tree, between the properties of the Pizza State
// pizzas and then data, loaded e looding for this particular scenario.

// Params: first: A reference to be used when selecting the piece of state you need ( products, in this case )
// second: A function that takes the state as a param and returns it.

// PIZZAS
export const getPizzaState = createSelector(
  fromFeature.getProductsState,
  (state: fromFeature.ProductsState) => state.pizzas
);

// PIZZAS.ENTITIES
export const getPizzasEntities = createSelector(
  getPizzaState,
  fromPizzas.getPizzasEntities
);

// GET SELECTED PIZZA BASED ON THE URL PARAM
export const getSelectedPizza = createSelector(
  getPizzasEntities, // from products
  fromRoot.getRouterState, // from Root
  (entities, router): Pizza => {
    return router.state && entities[router.state.params.pizzaId];
  }
);

// Returning all pizzas as an array for consumption
export const getAllPizzas = createSelector(getPizzasEntities, (entities) => {
  return Object.keys(entities).map((id) => entities[parseInt(id, 10)]);
});

// PIZZAS.LOADED
export const getPizzasLoaded = createSelector(
  getPizzaState,
  fromPizzas.getPizzasLoaded
);
// PIZZAS.LOADING
export const getPizzasLoading = createSelector(
  getPizzaState,
  fromPizzas.getPizzasLoading
);
