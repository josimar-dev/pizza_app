import { Injectable } from '@angular/core';

import { createEffect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import * as pizzaActions from '../actions/pizzas.action';
import * as fromServices from '../../services';
import { of } from 'rxjs';

/** EFFECTS  */
/**
 * Effects allow us to communicate with external source of data like servers,
 * we can execute operations outside of the Angular System.
 * Effects listen to Actions dispatched from the Store and execute
 * operations for the ones that need to do async stuff.
 * Once it gets a response back from a server, it dispatches a new action
 * to the store.
 * We basically take services out of components and use them with Effects,
 * so that our components don't have to request data via services directly
 *
 */

@Injectable()
export class PizzasEffects {
  constructor(
    private actions$: Actions,
    private pizzaService: fromServices.PizzasService
  ) {}

  /**
   * The createEffect method listens to actions and executes whatever operation
   * that is necessary based on the actions passed into the ofType call.
   * Then, a new action is dispatched to the store with a response from such operation ( asyn / effect )
   */

  loadPizzas$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Products] Load Pizzas'),
      mergeMap(() =>
        this.pizzaService.getPizzas().pipe(
          map((pizzas) =>
            pizzaActions.LOAD_PIZZAS_SUCCESS({ payload: pizzas })
          ),
          catchError((error) =>
            of(pizzaActions.LOAD_PIZZAS_FAIL({ payload: error }))
          ) // Needs to be returned as an observable
        )
      )
    )
  );
}
