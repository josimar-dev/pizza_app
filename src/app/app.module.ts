import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import {
  reducers,
  CustomSerializer,
  HydrationEffects,
  metaReducers,
} from './store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

// not used in production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// this would be done dynamically with webpack for builds
/*const environment = {
  development: true,
  production: false,
  api: 'http://localhost:3000',
};*/

// This is applied to every single reducer in the app
// In case the app is running in the production environment,
// the storeFreeze meta-reducer will be applied, preventing the store from
// being mutated anywhere in the app.
/*export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];*/

// routes
export const ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'products' },
  {
    path: 'products',
    loadChildren: () =>
      import('../products/products.module').then((m) => m.ProductsModule),
  },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule.forRoot(ROUTES),
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([HydrationEffects]),
    StoreRouterConnectingModule.forRoot(),
    // Instrumentation must be imported after the Store Module
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  // In order to be able to apply the CustomSerializer, we gotta provide to this module
  // the RouterStateSerializer that's gonna use it
  providers: [{ provide: RouterStateSerializer, useClass: CustomSerializer }],
  bootstrap: [AppComponent],
})
export class AppModule {}
