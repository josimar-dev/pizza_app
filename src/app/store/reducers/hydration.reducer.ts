import { Action, ActionReducer } from '@ngrx/store';
import * as HydrationActions from '../actions/hydration.actions';

function isHydrateSuccess(
  action: Action
): action is ReturnType<typeof HydrationActions.HYDRATE_SUCCESS> {
  return action.type === HydrationActions.HYDRATE_SUCCESS.type;
}

export const hydrationMetaReducer = (reducer) => {
  return (state, action) => {
    if (isHydrateSuccess(action)) {
      return action.state;
    } else {
      return reducer(state, action);
    }
  };
};
