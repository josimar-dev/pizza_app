import { createAction, props } from '@ngrx/store';

export const HYDRATE = createAction('[Hydration] Hydrate');
export const HYDRATE_SUCCESS = createAction(
  '[Hydration] Hydration Success',
  props<{ state: object }>()
);
export const HYDRATE_FAILURE = createAction('[Hydrate] Hydrate Failure');
